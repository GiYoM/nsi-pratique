def eratosthene(n):
    tableau = [True] * n
    tableau[0] = False
    tableau[1] = False
    for i in range(2, n):
        if tableau[i] == True:
            for multiple in range(2*i, n, i):
                tableau[multiple] = False
    return [i for i in range(n) if tableau[i]]


# Tests
assert eratosthene(2) == []
assert eratosthene(3) == [2]
assert eratosthene(7) == [2, 3, 5]
assert eratosthene(40) == [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37]
# Tests supplémentaires
for n in range(2, 100):
    reponse = sorted(list(set(range(2, n)) - {x for x in range(n)
                                              for y in range(2, x) if x % y == 0}))
    assert eratosthene(n) == reponse, f"{n}"
