# Commentaires

{{ py('exo_corr', 0, "# TESTS") }}

L'initialisation suit l'énoncé : on crée une liste nommée `tableau` dont toutes les valeurs sont à `True` sauf celles d'indices `0` et `1`.

On parcourt ensuite les indices entre `2` et `n-1` (inclus). Si `tableau[i]` est toujours égal `True`, `i` est premier. On "raye" ses multiples en mettant leur valeur à `False` dans `tableau`.

A la fin de la fonction on renvoie la liste par compréhension formée des indices des cellules de `tableau` contenant la valeur `True`.