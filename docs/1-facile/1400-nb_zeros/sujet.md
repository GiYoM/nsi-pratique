---
author: Franck Chambon
title: Nombre de zéros à la fin d'un entier
tags:
  - boucle
  - maths
---
# Nombre de zéros à la fin d'un entier

On souhaite avoir une fonction `nb_zeros` qui détermine le nombre de zéros à la fin de l'écriture décimale d'un entier $n>0$, très grand.

> **On s'interdit**, ici, d'utiliser la fonction de conversion `str`. Cette méthode est totalement inefficace avec des nombres très grands.

On demande plutôt de compter combien de fois on peut diviser un nombre par $10$ avec un reste égal à zéro.

!!! example "Exemples"

    ```pycon
    >>> nb_zeros(42000)
    3
    >>> nb_zeros(45321)
    0
    >>> nb_zeros(2020)
    1
    >>> nb_zeros(7**10000)
    0
    >>> nb_zeros(7**10000 * 1000)
    3
    ```

    Pour information, $7^{10000}$ est un nombre très grand qui ne finit sans aucun zéro.

{{ IDE('exo') }}
