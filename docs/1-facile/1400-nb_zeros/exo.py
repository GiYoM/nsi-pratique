def nb_zeros(n):
    ...




# tests

assert nb_zeros(42000) == 3
assert nb_zeros(45321) == 0
assert nb_zeros(2020) == 1
assert nb_zeros(7**10000) == 0
assert nb_zeros(7**10000 * 1000) == 3
